import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxGentelellaComponent } from './ngx-gentelella.component';

describe('NgxGentelellaComponent', () => {
  let component: NgxGentelellaComponent;
  let fixture: ComponentFixture<NgxGentelellaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgxGentelellaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxGentelellaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
