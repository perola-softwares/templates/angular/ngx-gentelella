import { Component, OnInit } from '@angular/core';


import 'fastclick';
import 'nprogress';
import 'gauge.js';
import 'skycons';
import 'flot';
import 'flot/source/jquery.flot.pie';
import 'flot/source/jquery.flot.time';
import 'flot/source/jquery.flot.stack';
import 'flot/source/jquery.flot.resize';
import 'flot-orderbars/js/jquery.flot.orderBars.js';
import 'flot-spline';
import 'flot.curvedlines';
import 'date.js';
import 'jqvmap';
import 'jqvmap/dist/maps/jquery.vmap.world';
import 'jqvmap/examples/js/jquery.vmap.sampledata';
import '@perolasoft/gentelella/build/js/custom.min.js';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ngx-gentelella-showcase';


  ngOnInit(): void {
    document.querySelector('body').classList.add('nav-md');
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

  }
}
