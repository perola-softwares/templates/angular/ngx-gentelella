import { TestBed } from '@angular/core/testing';

import { NgxGentelellaService } from './ngx-gentelella.service';

describe('NgxGentelellaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgxGentelellaService = TestBed.get(NgxGentelellaService);
    expect(service).toBeTruthy();
  });
});
