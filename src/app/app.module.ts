import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NgxGentelellaModule } from 'ngx-gentelella';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxGentelellaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
